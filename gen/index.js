/* eslint-disable no-await-in-loop */
const fs = require('fs');

const bbc = require('./sources/bbc');
const torrentfreak = require('./sources/torrentfreak');
const phoronix = require('./sources/phoronix');
const gamingonlinux = require('./sources/gamingonlinux');
const lxer = require('./sources/lxer');
const sky = require('./sources/sky');
const techradar = require('./sources/techradar');
const thehackernews = require('./sources/thehackernews');
const theverge = require('./sources/theverge');
const debugpoint = require('./sources/debugpoint');
const a9to5linux = require('./sources/a9to5linux');
const fossforce = require('./sources/fossforce');
const linuxlinks = require('./sources/linuxlinks');
const omgubuntu = require('./sources/omgubuntu');
const arstechnica = require('./sources/arstechnica');
const techcrunch = require('./sources/techcrunch');
const lwn = require('./sources/lwn');
const krebsonsecurity = require('./sources/krebsonsecurity');
const securityweek = require('./sources/securityweek');
const opensource = require('./sources/opensource');

const sources = [
  phoronix,
  bbc('news/technology'),
  sky('technology'),
  bbc('news/science_and_environment'),
  theverge('tech'),
  techradar('computing'),
  thehackernews,
  debugpoint,
  gamingonlinux,
  lxer,
  a9to5linux,
  fossforce,
  linuxlinks,
  omgubuntu,
  torrentfreak,
  arstechnica,
  techcrunch,
  lwn,
  krebsonsecurity,
  securityweek,
  opensource,
];

const main = async () => {
  const list = [];
  for (const s of sources) {
    try {
      const { source, sourceUrl, results } = await s.update();
      if (!results) {
        console.log('source:', source);
      } else {
        list.push({ source, sourceUrl, results: results.map(({ title, link }) => ({ link, title: title.trim() })) });
      }
    } catch (err) {
      console.log(s, err.message);
    }
  }
  if (!fs.existsSync('data')) fs.mkdirSync('data');
  fs.writeFileSync('data/news.json', JSON.stringify({ date: new Date().toISOString(), list }, null, 2));
};

main();

const xml2js = require('xml2js');

const parser = new xml2js.Parser();

const generate = (sub) => {
  const source = `techradar-${sub}`;

  const update = async () => {
    console.time(source);

    const response = await fetch(`https://www.techradar.com/rss/news/${sub}`, {
      headers: {
        'user-agent': 'Mozilla/5.0',
        accept: 'text/xml',
      },
    });
    const xmlText = await response.text();
    const jsonData = await parser.parseStringPromise(xmlText);

    const results = jsonData.rss.channel[0].item.map((a) => ({ title: a.title[0], link: a.link[0] })).splice(0, 10);

    console.timeEnd(source);
    return { source, sourceUrl: `https://www.techradar.com/uk/news/${sub}`, results };
  };

  return { update };
};

module.exports = generate;

if (require.main === module) {
  generate('computing')
    .update()
    .then((res) => console.log(res));
}

const xml2js = require('xml2js');

const parser = new xml2js.Parser();

const generate = (sub) => {
  const source = `theverge-${sub}`;

  const update = async () => {
    console.time(source);

    const response = await fetch(`https://www.theverge.com/${sub}/rss/index.xml`, {
      headers: {
        'user-agent': 'Mozilla/5.0',
        accept: 'text/xml',
      },
    });
    const xmlText = await response.text();

    const jsonData = await parser.parseStringPromise(xmlText);

    const results = jsonData.feed.entry
      .map((a) => ({ title: a.title[0], link: a.id[0] }))
      .reduce((acc, cur) => {
        const f = acc.find((a) => a.link === cur.link);
        return f ? acc : [...acc, cur];
      }, []);

    console.timeEnd(source);

    return { source, sourceUrl: `https://www.theverge.com/${sub}`, results: results.splice(0, 10) };
  };

  return { update };
};

module.exports = generate;

if (require.main === module) {
  generate('tech')
    .update()
    .then((res) => console.log(res));
}

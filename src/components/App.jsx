import React from 'react';
import { Container, Header, Segment } from 'semantic-ui-react';
// eslint-disable-next-line
import news from '../../data/news.json';

import Source from './Source';

export default function App() {
  const list = news.list.map((a) => (
    <Source key={a.source} source={a.source} sourceUrl={a.sourceUrl} results={a.results} />
  ));
  const lastUpdate = new Date(news.date);
  return (
    <Segment inverted color="black">
      <Container>
        <Segment basic>
          <Header inverted subheader={`Last updated ${lastUpdate.toLocaleString()}`} content="Nickel" />

          {list}
        </Segment>
      </Container>
    </Segment>
  );
}

import React from 'react';
import { List } from 'semantic-ui-react';

export default function Link(props) {
  const { link, title } = props;

  return (
    <List.Item as="a" href={link} target="_blank">
      {title}
    </List.Item>
  );
}

import React from 'react';
import { Header, List, Segment } from 'semantic-ui-react';

import SLink from './Link';

export default function Source(props) {
  const { source, sourceUrl, results } = props;

  const list = results.map((a) => <SLink key={a.link} link={a.link} title={a.title} />);

  console.log('sourceUrl', sourceUrl);

  return (
    <Segment>
      <Header href={sourceUrl} target="_blank">
        {source}
      </Header>
      <List divided relaxed>
        {list}
      </List>
    </Segment>
  );
}

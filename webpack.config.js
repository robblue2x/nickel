const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body',
});

module.exports = {
  entry: './src/index.jsx',
  mode: 'development', // Change to 'development'
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'index.[contenthash].js', // Use [contenthash] instead of [chunkhash]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react', '@babel/preset-env'],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [HtmlWebpackPluginConfig, new CleanWebpackPlugin()],
  devServer: {
    static: path.join(__dirname, 'public'), // Serve files from the 'public' directory
    compress: true,
    port: 3000, // Change to your preferred port
    hot: true, // Enable hot module replacement
    open: true, // Automatically open the browser
  },
};
